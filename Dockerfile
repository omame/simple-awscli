FROM python:alpine

RUN pip install --upgrade awscli

ENTRYPOINT [ "/bin/sh" ]
